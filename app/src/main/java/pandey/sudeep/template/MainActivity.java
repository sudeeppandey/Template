package pandey.sudeep.template;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.HorizontalScrollView;
import android.widget.TextView;

import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.SignInButton;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    private HorizontalScrollView scrollView;
    private GoogleSignInClient mGoogleSignInClient;
    private static int RC_SIGN_IN = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        scrollView = (HorizontalScrollView)findViewById(R.id.myScroll);

        TextView titleDisplay = (TextView)findViewById(R.id.title);
        titleDisplay.setText(BuildConfig.MainTitle);

        List<TextView> textViewContainer = new ArrayList<TextView>();
        textViewContainer.add((TextView)findViewById(R.id.title));
        textViewContainer.add((TextView) findViewById(R.id.version));
        textViewContainer.add((TextView) findViewById(R.id.fragment));
        textViewContainer.add((TextView) findViewById(R.id.service));
        textViewContainer.add((TextView) findViewById(R.id.broadcast));
        textViewContainer.add((TextView) findViewById(R.id.jobscheduler));
        textViewContainer.add((TextView) findViewById(R.id.multithreading));
        textViewContainer.add((TextView) findViewById(R.id.loader));
        textViewContainer.add((TextView) findViewById(R.id.recyclerView));
        textViewContainer.add((TextView) findViewById(R.id.persistence));
        textViewContainer.add((TextView) findViewById(R.id.provider));
        textViewContainer.add((TextView) findViewById(R.id.alarms));
        textViewContainer.add((TextView) findViewById(R.id.material));

        for (int i=0;i<textViewContainer.size();i++){
            UIUtils.autoSizeText(textViewContainer.get(i));
        }

        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .build();

        // Build a GoogleSignInClient with the options specified by gso.
        mGoogleSignInClient = GoogleSignIn.getClient(this, gso);

        // Set the dimensions of the sign-in button.
        SignInButton signInButton = findViewById(R.id.sign_in_button);
        signInButton.setSize(SignInButton.SIZE_STANDARD);
        signInButton.setOnClickListener(this);
    }

    protected void onStart(){
        super.onStart();
        // Check for existing Google Sign In account, if the user is already signed in
        // the GoogleSignInAccount will be non-null.
        GoogleSignInAccount account = GoogleSignIn.getLastSignedInAccount(this);
        updateUI(account);
    }


    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putIntArray("ARTICLE_SCROLL_POSITION",
                new int[]{ scrollView.getScrollX(), scrollView.getScrollY()});
    }

    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        final int[] position = savedInstanceState.getIntArray("ARTICLE_SCROLL_POSITION");
        if(position != null)
            scrollView.post(new Runnable() {
                public void run() {
                    scrollView.scrollTo(position[0], position[1]);
                }
            });
    }

    private void updateUI(GoogleSignInAccount _account){

    }

    public void onClick(View view){

        switch(view.getId()){
            case R.id.sign_in_button:
                signIn();
                break;
        }
    }

    private void signIn(){
        Intent signInIntent = mGoogleSignInClient.getSignInIntent();
        startActivityForResult(signInIntent, RC_SIGN_IN);
    }
}
